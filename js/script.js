class Card {
  constructor(post) {
    this.post = post;
  }

  render() {
    return (
      <div class="card">
        <h2 class="title">{this.post.title}</h2>
        <p class="text">{this.post.text}</p>
        <p class="author">
          Автор: {this.post.author.name} {this.post.author.surname}
        </p>
        <button class="delete">Видалити</button>
      </div>
    );
  }
}

const getPosts = async () => {
  const usersResponse = await fetch("https://ajax.test-danit.com/api/json/users");
  const users = await usersResponse.json();

  const postsResponse = await fetch("https://ajax.test-danit.com/api/json/posts");
  const posts = await postsResponse.json();

  return [...posts].map(post => {
    const author = users.find(user => user.id === post.authorId);
    return new Card({
      title: post.title,
      text: post.text,
      author: {
        name: author.name,
        surname: author.surname,
      },
    });
  });
};

const renderPosts = (posts) => {
  const postElements = posts.map(post => post.render());
  document.getElementById("posts").innerHTML = postElements.join("");
};

const deletePost = async (postId) => {
  const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
    method: "DELETE",
  });

  if (response.status === 200) {
    const postElement = document.querySelector(`.card[data-id="${postId}"]`);
    postElement.remove();
  }
};

document.addEventListener("DOMContentLoaded", async () => {
  const posts = await getPosts();
  renderPosts(posts);

  document.querySelectorAll(".delete").forEach(button => {
    button.addEventListener("click", async () => {
      const postId = button.dataset.id;
      await deletePost(postId);
    });
  });
});
